package autopark.domain;

/**
 * Created by vclass5 on 08.02.2016.
 */
public class Role {

    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName()   {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


